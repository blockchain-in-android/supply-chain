package dev.chandana.supplychain;

import android.content.Context;
import android.content.SharedPreferences;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.blockchain.Encryption;
import net.anandu.blocklib.consensus.proof_of_authority.PoAGenesisConfig;
import net.anandu.blocklib.database.BlockWithTransactions;
import net.anandu.blocklib.database.Transaction;
import net.anandu.blocklib.util.BlockCallback;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

//singleton class
public class BlockLibInit {
    private static BlockLib blockLib = null;
    private static trial field = null;
    private static onSyncInterface onsync_interface_obj = null;
    private static boolean makeBlock_isRunning;
    public static void setCallBack_addTransaction(trial object) {
        BlockLibInit.field = object;
    }
    public static void setCallBack_onSynComplete(onSyncInterface object) {BlockLibInit.onsync_interface_obj = object; }

    public static BlockLib getInstance(Context context) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException, InvalidKeyException, SignatureException {
        if (blockLib == null) {
            makeBlock_isRunning = false;
            blockLib = new BlockLib() {
                @Override
                public void onTransactionAdded(Transaction t) {
                    if (BlockLibInit.field != null) {
                        field.addTransactionCallBack(t);
                    }
                }

                @Override
                public void onConnected() {
                }

                @Override
                public void onSyncComplete() {
                    if(BlockLibInit.onsync_interface_obj != null && !makeBlock_isRunning)
                        onsync_interface_obj.onSyncComplete_callback();
                }

                @Override
                public void onBlockAdded(Integer blockNum) {
                }
            };
            SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            String publicKey = sharedPref.getString(context.getString(R.string.public_key), "");
            String privateKey = sharedPref.getString(context.getString(R.string.private_key), "");
            Encryption encryption;
            KeyPair key;
            if (privateKey.equals("") || publicKey.equals("")) {
//            Log.d(TAG, "BlockLibInit: Generating new Key Pair");
                encryption = new Encryption();
                privateKey = encryption.getPrivateKeyString();
                publicKey = encryption.getPubKeyString();
                key = encryption.getKey();

                // Save the publicKey and privateKey
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(context.getString(R.string.private_key), privateKey);
                editor.putString(context.getString(R.string.public_key), publicKey);
                editor.apply();
            } else {
                key = Encryption.getKeyPairFromEncoded(publicKey, privateKey);
            }

            String peerId = Encryption.getAddress(key);
            //should provide a list of validators, how??
            //one validator.
            List<String> validators = new ArrayList<>();
            validators.add(peerId);
            PoAGenesisConfig genesisConfig = new PoAGenesisConfig(validators, null, 20);
            //TODO: remove this hardcoded checking userrole code
            //TODO: to fix - sync the authority nodes table as well - consensus layer
            //blockLib.onSyncComplete();
            String userrole = sharedPref.getString(context.getString(R.string.user_role), "");
            blockLib.init(context, peerId, key, genesisConfig, t -> true, false);
            //TODO: add onsync function ??
            String address = Encryption.getAddress(key);
            BlockLibInit.setCallBack_onSynComplete(()->{
                System.out.println("---ON SYNC COMPLETE CALLED---");
                blockLib.checkIfAuthorityExists(address, (exists)->{
                    if(exists) {
                        System.out.println("AM I AUTHORITY NODE:" + exists);
                        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
                        exec.scheduleAtFixedRate(() -> {
                            try {
                                System.out.println("Address: " + address);
                                System.out.println("Creating block now!!!");
                                blockLib.createBlock(
                                        new HashMap<>());
                            } catch (Exception e) {
                                System.err.println(e);
                            }
                        }, 10, 30, TimeUnit.SECONDS);
                    }
                    makeBlock_isRunning = true;
                });
            });
//            if("Producer".equals(userrole))
//                blockLib.init(context, peerId, key, genesisConfig, t -> true, false);
//            else
//                blockLib.init(context, peerId, key, genesisConfig, t -> true,true);
            //checking if the guy's validator
//            if("Producer".equals(userrole)) {
//                System.out.println("I AM A VALIDATOR: WITH HASH : " + peerId);
//                ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
//                exec.scheduleAtFixedRate(() -> {
//                    try {
//                        System.out.println("MY USER ROLE: " + userrole);
//                        System.out.println("ADDRESS: " + address);
//                        System.out.println("Creating block now!!!");
//                        blockLib.createBlock(
//                                new HashMap<>());
//                    } catch (Exception e) {
//                        System.err.println(e);
//                    }
//                }, 0, 60, TimeUnit.SECONDS);
//            }
        }
        return blockLib;
    }

    public interface trial {
        void addTransactionCallBack(Transaction t);
    }

    public interface onSyncInterface {
        void onSyncComplete_callback();
    }

}
