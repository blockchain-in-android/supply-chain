package dev.chandana.supplychain;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.progressindicator.LinearProgressIndicator;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.blockchain.Encryption;
import net.anandu.blocklib.database.Transaction;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        if(sharedPreferences.contains(getString(R.string.public_key))) {
            //String userRole = sharedPreferences.getString(getString(R.string.user_role), null);
            String userPubKey = sharedPreferences.getString(getString(R.string.public_key), null);
            String userPrivKey = sharedPreferences.getString(getString(R.string.private_key), null);
            //String userPrivKey = "hello";
            //Toast.makeText(context, "User Role: " + userRole + "\nUser Public Key: " + userPubKey, Toast.LENGTH_SHORT).show();
            //System.out.println("User Role " + userRole + "User Public Key " + userPubKey);
            //check if the role is producer
            //if (role is producer)
            //check if the pub key/address(check with this) exists in the genesis block
                //if yes then navigate else generate error and clear the file contents(?)(YET TO DECIDE)
            //else
            //just navigate to the next screen
            System.out.println("Public key: " + userPubKey + "\n Private Key: " + userPrivKey);
            Intent intent = new Intent(context, LandingPage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(intent);
        } else {
            SharedPreferences.Editor editor = sharedPreferences.edit();

            String[] supportedRoles = getResources().getStringArray(R.array.userroles);
            ArrayAdapter<AutoCompleteTextView> adapter = new ArrayAdapter(this, R.layout.dropdown_menu, supportedRoles);
            AutoCompleteTextView autoCompleteTextView = findViewById(R.id.spinner_userrole);
            autoCompleteTextView.setAdapter(adapter);

            autoCompleteTextView.setOnItemClickListener((parent, view, pos, id) -> {
                Object item = parent.getItemAtPosition(pos);
                if (item instanceof String) {
                    System.out.println(item);
                    editor.putString(getString(R.string.user_role), item.toString());
                    editor.apply();
                    //Toast.makeText(context, "user role stored" + item, Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(MainActivity.this, landingPage.class);
//                        startActivity(intent);
                }
            });

            //ask user to select the role and generate/choose the keypair, based on the option
            Button generateKeyPairButton = (Button)findViewById(R.id.generate_keypair);
            LinearProgressIndicator linearProgressIndicator = findViewById(R.id.loading_indicator);
            generateKeyPairButton.setOnClickListener(v -> {
                try {
                    linearProgressIndicator.setVisibility(View.VISIBLE);
                    generateKeyPairButton.setEnabled(false);
                    Encryption encryption = new Encryption();
                    String publicKey = encryption.getPubKeyString();
                    String privateKey = encryption.getPrivateKeyString();
                    editor.putString(getString(R.string.public_key), publicKey);
                    editor.putString(getString(R.string.private_key), privateKey);
                    editor.apply();
                    //Toast.makeText(context,"public key: " + publicKey + " private key: " + privateKey,Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, LandingPage.class);
                    startActivity(intent);
                } catch(Exception e) {
                    System.out.println(e);
                }
            });
        }
        //TODO: [5]: add "provide file" button. (secondary).
    }
}
