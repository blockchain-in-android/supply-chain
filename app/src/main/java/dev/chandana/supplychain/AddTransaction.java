package dev.chandana.supplychain;

import androidx.activity.result.ActivityResultLauncher;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.client.android.Intents;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.blockchain.Encryption;
import net.anandu.blocklib.util.StringUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AddTransaction extends AppCompatActivity{
    String scannedReceiverAddress;

    private final ActivityResultLauncher<ScanOptions> barcodeLauncher = registerForActivityResult(new ScanContract(),
            result -> {
                if (result.getContents() == null) {
                    Intent originalIntent = result.getOriginalIntent();
                    if (originalIntent == null) {
                        Log.d("MainActivity", "Cancelled scan");
                        Toast.makeText(AddTransaction.this, "Cancelled", Toast.LENGTH_LONG).show();
                    } else if (originalIntent.hasExtra(Intents.Scan.MISSING_CAMERA_PERMISSION)) {
                        Log.d("MainActivity", "Cancelled scan due to missing camera permission");
                        Toast.makeText(AddTransaction.this, "Cancelled due to missing camera permission", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.d("MainActivity", "Scanned");
                    Toast.makeText(AddTransaction.this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                    String[] parts = result.getContents().split(":");
                    if(parts[0].equals("addr") && parts.length == 2) {
                        AddTransaction.this.scannedReceiverAddress= parts[1];
                        TextInputLayout receiverAddressEditText =  findViewById(R.id.receiverAddress);
                        Objects.requireNonNull(receiverAddressEditText.getEditText()).setText(this.scannedReceiverAddress, TextView.BufferType.EDITABLE);
                    } else {
                        Log.d("MainActivity", "Invalid QR");
                        Toast.makeText(AddTransaction.this, "Invalid QR", Toast.LENGTH_LONG).show();
                    }
                }
            });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);

        // Scan Address
        ScanOptions options = new ScanOptions();
        options.setDesiredBarcodeFormats(ScanOptions.QR_CODE);
        barcodeLauncher.launch(options);

        Context c = getApplicationContext();
        Intent intent = getIntent();
        String userRole = "";

        // check intent is null or not
        if(intent != null){
            userRole = intent.getStringExtra("user_role");
        } else{
            Toast.makeText(c, "Intent is null", Toast.LENGTH_SHORT).show();
        }
        BlockLib blockLib;
        try {
            SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
            String publicKey = sharedPref.getString(c.getString(R.string.public_key), null);
            String privateKey = sharedPref.getString(c.getString(R.string.private_key), null);
            blockLib = BlockLibInit.getInstance(c);

            String[] supportedTransactionTypes= getResources().getStringArray(R.array.transactiontypes);
//            System.out.println("----" + Arrays.toString(supportedTransactionTypes) + "-----");
//            System.out.println("USER ROLE: " + userRole);
            //TODO ---------
            if("Wholesaler".equals(userRole) || "Retailer".equals(userRole)) {
                supportedTransactionTypes = new String[]{getString(R.string.purchase), getString(R.string.end_purchase)};
            }
//            System.out.println("----" + Arrays.toString(supportedTransactionTypes) + "-----");
            // filter based on userType

            ArrayAdapter<AutoCompleteTextView> adapter = new ArrayAdapter(this, R.layout.dropdown_menu, supportedTransactionTypes);
            AutoCompleteTextView autoCompleteTextView = findViewById(R.id.transaction_type);
            autoCompleteTextView.setAdapter(adapter);


            Button addTransactionButton = (Button) findViewById(R.id.add_transaction_button2);
            addTransactionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //create a hashmap and
                    TextInputLayout receiver_address_editText =  findViewById(R.id.receiverAddress);
                    String receiverAddress = receiver_address_editText.getEditText().getText().toString();
                    String transactionType = autoCompleteTextView.getText().toString();
                    EditText lot_number_editText = (EditText) findViewById(R.id.lot_id);
                    String lotNumber = lot_number_editText.getText().toString();
                    EditText product_key_editText = (EditText) findViewById(R.id.product_key);
                    String productKey = product_key_editText.getText().toString();
                    Map<String,String> data= new HashMap<>();
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("lot_id",lotNumber);
                        obj.put("product_keys", productKey);
                        obj.put("purchase_type", "ASSORTMENT");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    data.put("transaction_type", transactionType);
                    data.put("details",obj.toString());
                    try {
                        blockLib.createTransaction(Encryption.getAddress(Encryption.getKeyPairFromEncoded(publicKey, privateKey)), receiverAddress, null, data);
                        Toast.makeText(c, "Transaction Added!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    catch (Exception e) {
                        throw new RuntimeException("create transaction failed");
                    }
                    //Toast.makeText(c,a,Toast.LENGTH_SHORT).show();
                }
            });
        } catch(Exception e) {
            System.out.println(e);
        }

    }
}