package dev.chandana.supplychain;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.blockchain.Encryption;
import net.anandu.blocklib.database.BlockWithTransactions;
import net.anandu.blocklib.database.Transaction;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import dev.chandana.supplychain.addTransaction.SelectTransactionType;

public class LandingPage extends AppCompatActivity {

    BlockLib blockLib;
    String userAddress;
    ArrayList<String> toAddress;
    ArrayList<String> timeStamp;
    ArrayList<String> ttype;
    ArrayList<String> transactionIds;
    MyListAdapter adapter;
    private Toolbar toolbar;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.refreshButton) {
            DateFormat df = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
            if (timeStamp.size() > 0) {
                //System.out.println("LAST ELEMENT OF TIMESTAMP ARRAY: " + timeStamp.get(0));
                try {
                    Date date = df.parse(timeStamp.get(0));
                    assert date != null;
                    blockLib.getTransactionsCreatedAfterTime((Long) date.toInstant().getEpochSecond(), (transactionList) -> {
                        int size = timeStamp.size();
                        transactionList.forEach((Transaction t) -> {
                            System.out.println("REFRESHING THE DATA: ");
                            System.out.println("DATE: " + date.toInstant().getEpochSecond());
                            System.out.println("TRANSACTION " + t);
                            System.out.println("TIME STAMP OF THIS TRANSACTION: " + t.getTimestamp());
                            transactionIds.add(0,t.getTransactionId());
                            toAddress.add(0,t.getReceiver());
                            Instant instant = Instant.ofEpochSecond(t.getTimestamp());
                            timeStamp.add(0, df.format(Date.from(instant)));
                            ttype.add(0, t.getData().get("transaction_type"));
                        });
                    });
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            } else {
                blockLib.getTransactions(30, (transactionList) -> transactionList.forEach((Transaction t) -> {
                    System.out.println(t);
                    transactionIds.add(t.getTransactionId());
                    toAddress.add(t.getReceiver());
                    Instant instant = Instant.ofEpochSecond(t.getTimestamp());
                    timeStamp.add(df.format(Date.from(instant)));
                    ttype.add(t.getData().get("transaction_type"));
                }));
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
            Toast.makeText(LandingPage.this, "Refresh successful", Toast.LENGTH_SHORT).show();
        }
        if (id == R.id.showQRcodeButton) {
            Context context = getApplicationContext();
            Intent intent = new Intent(context, ViewAddress.class);
            intent.putExtra("address", userAddress);
            startActivity(intent);
        }
        if (id == R.id.exportButton) {

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ArrayList<Transaction> transactions = new ArrayList<>();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);
        Context c = getApplicationContext();
        //initialising objects
        this.toAddress = new ArrayList<>();
        this.timeStamp = new ArrayList<>();
        this.ttype = new ArrayList<>();
        this.transactionIds = new ArrayList<>();
        this.adapter = new MyListAdapter(this, toAddress, timeStamp, ttype);

        //toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.showOverflowMenu();
//        toolbar.setNavigationIcon(R.drawable.ic_refresh);
//        toolbar.setNavigationIcon(R.drawable.ic_show_qrcode);
//        getSupportActionBar().setIcon(R.drawable.ic_refresh);
//        getSupportActionBar().setIcon(R.drawable.ic_show_qrcode);
        //BlockLib.init()
        try {
            blockLib = BlockLibInit.getInstance(c);
            userAddress = Encryption.getAddress(blockLib.getKeyPair());
        } catch (Exception e) {
            //printing exact error log
            e.printStackTrace();
            throw new RuntimeException(e);
            //throw new RuntimeException("block lib failed");
        }

        //shared preferences
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        String userRole = sharedPref.getString(getString(R.string.user_role), null);
        //add button - add transaction
        FloatingActionButton addTransactionButton = (FloatingActionButton) findViewById(R.id.add_transaction_button);
        if("Consumer".equals(userRole)) {
            addTransactionButton.setVisibility(View.INVISIBLE);
        }
        addTransactionButton.setOnClickListener(view -> {
            //Toast.makeText(c,"add transaction",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(c, SelectTransactionType.class);
            intent.putExtra("user_role", userRole);
            startActivity(intent);
        });
        FloatingActionButton searchButton = (FloatingActionButton) findViewById(R.id.search_button);
        searchButton.setOnClickListener(view -> {
            //Toast.makeText(c,"add transaction",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(c, ProductSupplyChain.class);
            startActivity(intent);
        });
        //list view - list transactions
        DateFormat df = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

        ListView listView = (ListView) findViewById(R.id.transactions_list_id);
        listView.setAdapter(adapter);
        blockLib.getTransactions(30, (transactionList) -> {
            transactionList.forEach((Transaction t) -> {
                transactions.add(t);
                System.out.println(t);
                transactionIds.add(t.getTransactionId());
                toAddress.add(t.getReceiver());
                Instant instant = Instant.ofEpochSecond(t.getTimestamp());
                timeStamp.add(df.format(Date.from(instant)));
                ttype.add(t.getData().get("transaction_type"));
            });
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
        });
        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            Intent intent = new Intent(LandingPage.this, TransactionPage.class);
            // todo: fetch the actual TransactionId
            intent.putExtra("transaction", transactions.get(i));
            startActivity(intent);
        });

        //call back to be called when an new transaction's added
        //adding values to arraylist
        //TODO: Perform this on a seperate thread.
        BlockLibInit.setCallBack_addTransaction((newTransaction) -> {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            StackTraceElement element = stackTrace[2];
            System.out.println("I was called by a method named: " + element.getMethodName());
            System.out.println("That method is in class: " + element.getClassName());
            System.out.println("-----------ADD TRANSACTION CALLBACK--------------");
            System.out.println("---Calculated Hash---::" + newTransaction.calculateHash());
            toAddress.add(0,newTransaction.getReceiver());
            Instant instant = Instant.ofEpochSecond(newTransaction.getTimestamp());
            timeStamp.add(0,df.format(Date.from(instant)));
            transactionIds.add(0, newTransaction.getTransactionId());
            transactions.add(0,newTransaction);
            //TODO: remove hardcoded purchase and fetch the transaction type from the transaction
            //ttype.add("PURCHASE");
            JSONObject json = new JSONObject(newTransaction.getData());
            //transactionType = newTransaction.getData();
            try {
                ttype.add(0,json.getString("transaction_type"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
        });

        //TODO:[1] list all transactions.(change activity name)
        //TODO:[2]: Loading screen
        //TODO:[3]: QR code scanner
        //TODO:[4] : Activity showing the transaction details.
    }
}
