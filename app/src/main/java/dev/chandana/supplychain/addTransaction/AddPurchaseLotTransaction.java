package dev.chandana.supplychain.addTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.client.android.Intents;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.blockchain.Encryption;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import dev.chandana.supplychain.AddTransaction;
import dev.chandana.supplychain.BlockLibInit;
import dev.chandana.supplychain.R;

public class AddPurchaseLotTransaction extends AppCompatActivity {

    private TextInputEditText receiverAddressTextInputEditText;
    private TextInputEditText locationTextInputEditText;
    private TextInputEditText lotIdTextInputEditText;
    private TextInputEditText productKeysTextInputEditText;

    private String getUserAddress(Context c) {
        try {
            SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
            String publicKey = sharedPref.getString(c.getString(R.string.public_key), null);
            String privateKey = sharedPref.getString(c.getString(R.string.private_key), null);
            return Encryption.getAddress(Encryption.getKeyPairFromEncoded(publicKey, privateKey));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "INVALID_KEY";
    }

    private final ActivityResultLauncher<ScanOptions> barcodeLauncher = registerForActivityResult(new ScanContract(),
            result -> {
                if (result.getContents() == null) {
                    Intent originalIntent = result.getOriginalIntent();
                    if (originalIntent == null) {
                        Log.d("MainActivity", "Cancelled scan");
                        Toast.makeText(AddPurchaseLotTransaction.this, "Cancelled", Toast.LENGTH_LONG).show();
                    } else if (originalIntent.hasExtra(Intents.Scan.MISSING_CAMERA_PERMISSION)) {
                        Log.d("MainActivity", "Cancelled scan due to missing camera permission");
                        Toast.makeText(AddPurchaseLotTransaction.this, "Cancelled due to missing camera permission", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.d("MainActivity", "Scanned");
                    Toast.makeText(AddPurchaseLotTransaction.this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                    String[] parts = result.getContents().split(":");
                    if(parts[0].equals("addr") && parts.length == 2) {
                        AddPurchaseLotTransaction.this.receiverAddressTextInputEditText.setText(parts[1]);
                        Toast.makeText(AddPurchaseLotTransaction.this, "Scanned Receiver Address", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d("MainActivity", "Invalid QR");
                        Toast.makeText(AddPurchaseLotTransaction.this, "Invalid QR", Toast.LENGTH_LONG).show();
                    }
                }
            });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_purchase_transaction);
        Context c = getApplicationContext();
        String userAddress = getUserAddress(c);
        receiverAddressTextInputEditText = findViewById(R.id.receiverAddress);
        locationTextInputEditText = findViewById(R.id.location);
        lotIdTextInputEditText = findViewById(R.id.lotId);
        productKeysTextInputEditText = findViewById(R.id.productKey);
        Button addPurchaseButton = findViewById(R.id.add_purchase_txn);
        Button receiverAddressScanQRButton = findViewById(R.id.receiverAddress_scan_qr);

        ScanOptions options = new ScanOptions();
        options.setDesiredBarcodeFormats(ScanOptions.QR_CODE);
        options.setOrientationLocked(false);
        receiverAddressScanQRButton.setOnClickListener(view -> barcodeLauncher.launch(options));
        addPurchaseButton.setOnClickListener(view -> {
            addPurchaseButton.setEnabled(false);
            String receiverAddress = receiverAddressTextInputEditText.getEditableText().toString();
            String location = locationTextInputEditText.getEditableText().toString();
            String lotId = lotIdTextInputEditText.getEditableText().toString();
            String productKeys = productKeysTextInputEditText.getEditableText().toString();

            try {
                Map<String, String> obj = new HashMap<>();
                obj.put("lot_id", lotId);
                obj.put("product_keys", productKeys);
                obj.put("purchase_type", "ASSORTMENT");
                obj.put("transaction_type", "Purchase");
                obj.put("location", location);
                BlockLib blockLib = BlockLibInit.getInstance(c);
                assert blockLib != null;
                blockLib.createTransaction(userAddress, receiverAddress, null, obj);
                finish();
            } catch (Exception e) {
                e.printStackTrace();
            }

        });

    }

}
