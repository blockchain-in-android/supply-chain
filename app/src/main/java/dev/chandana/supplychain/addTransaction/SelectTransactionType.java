package dev.chandana.supplychain.addTransaction;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import dev.chandana.supplychain.R;

public class SelectTransactionType extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_type_picker);

        Context c = getApplicationContext();
        Intent intent = getIntent();
        String userRole = "";

        //get the userRole through intent
        if ( intent != null) {
            userRole = intent.getStringExtra("user_role");
        } else {
            Toast.makeText(c, "Failed to extract userRole", Toast.LENGTH_SHORT).show();
        }

        if ( "Consumer".equals(userRole) ) {
            // directly take to end Purchase screen
            Intent targetIntent = new Intent(c, AddEndPurchaseTransaction.class);
            startActivity(targetIntent);
            finish();
        }

        Button createLotTransactionButton = findViewById(R.id.create_lot_txn);
        Button purchaseButton = findViewById(R.id.purchase_txn);
        Button endPurchaseButton = findViewById(R.id.end_purchase_txn);

        if ( !"Producer".equals(userRole) ) {
            createLotTransactionButton.setVisibility(View.INVISIBLE);
        }
        // add listener to all buttons
        createLotTransactionButton.setOnClickListener(view -> {
            Intent targetIntent = new Intent(c, AddCreateLotTransaction.class);
            startActivity(targetIntent);
        });
        purchaseButton.setOnClickListener(view -> {
            Intent targetIntent = new Intent(c, AddPurchaseLotTransaction.class);
            startActivity(targetIntent);
        });
        endPurchaseButton.setOnClickListener(view -> {
            Intent targetIntent = new Intent(c, AddEndPurchaseTransaction.class);
            startActivity(targetIntent);
        });


    }
}
