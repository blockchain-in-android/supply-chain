package dev.chandana.supplychain.addTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.blockchain.Encryption;


import java.util.HashMap;
import java.util.Map;

import dev.chandana.supplychain.BlockLibInit;
import dev.chandana.supplychain.R;

public class AddCreateLotTransaction extends AppCompatActivity {
    private TextInputEditText manufacturedDateTextInputEditText;
    private TextInputEditText expiryDateTextInputEditText;
    private TextInputEditText locationTextInputEditText;
    private TextInputEditText lotIdTextInputEditText;
    private TextInputEditText productKeysTextInputEditText;

    private String getUserAddress(Context c) {
        try {
            SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
            String publicKey = sharedPref.getString(c.getString(R.string.public_key), null);
            String privateKey = sharedPref.getString(c.getString(R.string.private_key), null);
            return Encryption.getAddress(Encryption.getKeyPairFromEncoded(publicKey, privateKey));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "INVALID_KEY";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_create_lot_transaction);

        Context c = getApplicationContext();
        String userAddress = getUserAddress(c);
        manufacturedDateTextInputEditText = findViewById(R.id.manufacturedDate);
        expiryDateTextInputEditText = findViewById(R.id.expiryDate);
        locationTextInputEditText = findViewById(R.id.location);
        lotIdTextInputEditText = findViewById(R.id.lotId);
        productKeysTextInputEditText = findViewById(R.id.productKey);

        Button createTransactionButton = findViewById(R.id.add_create_lot_txn);
        createTransactionButton.setOnClickListener(view -> {
            createTransactionButton.setEnabled(false);
            String manufacturedDate = manufacturedDateTextInputEditText.getEditableText().toString();
            String expiryDate = expiryDateTextInputEditText.getEditableText().toString();
            String location = locationTextInputEditText.getEditableText().toString();
            String lotId = lotIdTextInputEditText.getEditableText().toString();
            String productKeys = productKeysTextInputEditText.getEditableText().toString();

            try {
                Map<String, String> obj = new HashMap<>();
                obj.put("lot_id", lotId);
                obj.put("product_keys", productKeys);
                obj.put("purchase_type", "ASSORTMENT");
                obj.put("transaction_type", "Create LOT");
                obj.put("location", location);
                obj.put("expiry_date", expiryDate);
                obj.put("manufactured_date", manufacturedDate);
                BlockLib blockLib = BlockLibInit.getInstance(c);
                assert blockLib != null;
                blockLib.createTransaction(userAddress,"", null, obj);
                finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


    }
}
