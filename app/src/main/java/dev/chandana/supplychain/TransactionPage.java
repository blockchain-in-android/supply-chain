package dev.chandana.supplychain;

import com.google.gson.Gson;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import net.anandu.blocklib.database.Transaction;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Map;

public class TransactionPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_info);
        Context c = getApplicationContext();
        Transaction transaction = null;
        if (getIntent().getExtras() != null) {
            transaction = (Transaction) getIntent().getSerializableExtra("transaction");
        } else {
            Toast.makeText(c, "Intent is null", Toast.LENGTH_SHORT).show();
        }
        // query blockLib and get the transaction
        if (transaction != null) {
            DateFormat df = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
            //printing transaction
            System.out.println(transaction);
            //getting all text views
            TextView transaction_id = (TextView) findViewById(R.id.transaction_id);
            TextView sender_address = findViewById(R.id.sender_address);
            TextView receiver_address = findViewById(R.id.receiver_address);
            TextView time_stamp = findViewById(R.id.time_stamp);
            TextView transaction_type = findViewById(R.id.transaction_type);
            TextView lot_id = findViewById(R.id.lot_id);
            TextView product_keys = findViewById(R.id.product_keys);
            TextView text_manufactured_date = findViewById(R.id.text_manufactured_date);
            TextView manufactured_date = findViewById(R.id.manufactured_date);
            TextView text_expiry_date = findViewById(R.id.text_expiry_date);
            TextView expiry_date = findViewById(R.id.expiry_date);
            TextView location = findViewById(R.id.location);

            //setting the values
            transaction_id.setText(transaction.getTransactionId());
            sender_address.setText(transaction.getSender());
            receiver_address.setText(transaction.getReceiver());
            time_stamp.setText(df.format(Date.from(Instant.ofEpochSecond(transaction.getTimestamp()))));
            //TODO: set location
            //TODO: if create_lot, set manufacturing date and expiry date
            Map<String, String> data_map = transaction.getData();
            transaction_type.setText(data_map.get("transaction_type"));
            location.setText(data_map.get("location"));
            lot_id.setText(data_map.get("lot_id"));
            product_keys.setText(data_map.get("product_keys"));
            if("Create LOT".equals(data_map.get("transaction_type"))) {
                text_expiry_date.setVisibility(View.VISIBLE);
                text_manufactured_date.setVisibility(View.VISIBLE);
                expiry_date.setVisibility(View.VISIBLE);
                expiry_date.setText(data_map.get("expiry_date"));
                manufactured_date.setVisibility(View.VISIBLE);
                manufactured_date.setText(data_map.get("manufactured_date"));
            }
        }
    }
}
