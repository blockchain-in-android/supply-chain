package dev.chandana.supplychain;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MyListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final ArrayList<String> toAddress;
    private final ArrayList<String> timeStamp;
    private final ArrayList<String> ttype;

    public MyListAdapter(Activity Context, ArrayList<String> toAddress, ArrayList<String> timeStamp, ArrayList<String> ttype) {
        super(Context, R.layout.list, toAddress);
        this.context = Context;
        this.toAddress = toAddress;
        this.timeStamp = timeStamp;
        this.ttype = ttype;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list, null, true);

        TextView toAddress = (TextView) rowView.findViewById(R.id.toAddress);
        TextView timeStamp = (TextView) rowView.findViewById(R.id.timeStamp);
        TextView ttype = (TextView) rowView.findViewById(R.id.ttype);

        String toAddressString = this.toAddress.get(position);
        toAddressString = toAddressString.length() > 15 ? toAddressString.substring(0, 15) + "..." : toAddressString;

        toAddress.setText(toAddressString);
        timeStamp.setText(this.timeStamp.get(position));
        ttype.setText(this.ttype.get(position));

        return rowView;
    }
}
