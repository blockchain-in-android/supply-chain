package dev.chandana.supplychain;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.database.Transaction;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class ProductSupplyChain extends AppCompatActivity {

    String lotNumber = "0";
    String productKey = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_supply_chain);
        Context c = getApplicationContext();
        BlockLib blockLib;
        try {
           blockLib = BlockLibInit.getInstance(c);
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
        //UI
        List<Transaction> transactions = new ArrayList<>();
        ArrayList<String> toAddress = new ArrayList<>();
        ArrayList<String> timeStamp = new ArrayList<>();
        ArrayList<String> ttype = new ArrayList<>();
        ArrayList<String> transactionIds = new ArrayList<>();
        //list view - list transactions
        DateFormat df = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

        EditText lot_number_editText = (EditText) findViewById(R.id.lot_id);
        EditText product_key_editText = (EditText) findViewById(R.id.product_key);
        Button searchButton = (Button) findViewById(R.id.search);
        ListView listView = (ListView)findViewById(R.id.search_transactions_list_id);
        MyListAdapter adapter = new MyListAdapter(this,toAddress,timeStamp,ttype);
        listView.setAdapter(adapter);
        //making the list clickable
        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            Intent intent = new Intent(ProductSupplyChain.this, TransactionPage.class);
            // todo: fetch the actual TransactionId
            System.out.println("TRANSACTION_ID OF THE CLICKED TRANSACTION : " + transactionIds.get(i));
            intent.putExtra("transaction", transactions.get(i));
            startActivity(intent);
        });
        //setting the text view as not visible
        TextView tv = (TextView) findViewById(R.id.textView3);
        tv.setVisibility(View.GONE);
        //logic
        searchButton.setOnClickListener(view -> {
            lotNumber = lot_number_editText.getText().toString();
            productKey = product_key_editText.getText().toString();
            System.out.println("LOT NUMBER: " + lotNumber + "\nPRODUCT KEY: " + productKey);
//            String query = "\"lot_id\\" +"\":\\" + "\"" + lotNumber + "\\" + "\"";
            blockLib.searchInTransactionData(lotNumber, (transactionList)->{
                System.out.println(transactionList);
                String regex = ".*\"product_keys\":"+".*[,\"]"+ productKey +"[,\"].*";
                System.out.println("REGEX: " + regex);
                System.out.println("LIST OF TRANSACTIONS WITH MATCHING QUERY");
                System.out.println(transactionList);
                transactionList.removeIf(s -> {
                    System.out.println(s.getData());
                    String productKeys= s.getData().get("product_keys");
                    assert productKeys != null;
                    return !productKeys.contains(productKey);
                    //System.out.println("REGEX:" + regex);
//                    return !details.matches(regex);
                });
                System.out.println("FILTERED TRANSACTIONS" + transactionList);
                transactions.clear();
                toAddress.clear();
                transactionIds.clear();
                timeStamp.clear();
                ttype.clear();
                transactionList.forEach(t -> {
                    transactions.add(t);
                    transactionIds.add(t.getTransactionId());
                    toAddress.add(t.getReceiver());
                    Instant instant = Instant.ofEpochSecond(t.getTimestamp());
                    timeStamp.add(df.format(Date.from(instant)));
                    ttype.add(t.getData().get("transaction_type"));
                });
                //filter the transactions based on the product key match
                //assign this filtered list to list
                //refresh the object
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(transactionList.isEmpty()) {
                            tv.setVisibility(View.VISIBLE);
                        } else {
                            tv.setVisibility(View.GONE);
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
            });
        });
    }
}