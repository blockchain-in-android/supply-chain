package dev.chandana.supplychain;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class ViewAddress extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_address);

        Intent intent = getIntent();
        String userAddress = intent.getStringExtra("address");

        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            String data = getString(R.string.AddressPrefix) + userAddress;
            Bitmap bitmap = barcodeEncoder.encodeBitmap(data, BarcodeFormat.QR_CODE, 400, 400);
            ImageView imageViewQrCode = (ImageView) findViewById(R.id.userAddressQR);
            imageViewQrCode.setImageBitmap(bitmap);
        } catch(Exception e) {
            finish();
        }
    }
}